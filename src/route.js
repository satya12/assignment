import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Main from './mainComponent';
import PrintDemo from './printDemo'

export default function RouterDemo() {
 
     return (
       <Router>
         <div>

           <Switch>
             <Route exact path="/">
               <Main />
             </Route>
             <Route path="/print_url">
               <PrintDemo />
             </Route>
            
           </Switch>
         </div>
       </Router>
     );
   }
  
   
      
  


